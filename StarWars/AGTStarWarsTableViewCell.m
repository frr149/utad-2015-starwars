//
//  AGTStarWarsTableViewCell.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 28/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTStarWarsTableViewCell.h"

@implementation AGTStarWarsTableViewCell

+(NSString*) cellId{
    return [[self class]description];
}

+(CGFloat) height{
    return 60;
}
- (void)awakeFromNib {
    // Initialization code

    self.characterPhotoView.layer.cornerRadius = 30;
    
}


-(void) prepareForReuse{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
