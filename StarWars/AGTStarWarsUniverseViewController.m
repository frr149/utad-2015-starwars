//
//  AGTStarWarsUniverseViewController.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 07/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTStarWarsUniverseViewController.h"
#import "AGTCharacterViewController.h"
#import "AGTStarWarsTableViewCell.h"

@interface AGTStarWarsUniverseViewController ()
@property (nonatomic, strong) AGTStarWarsUniverse *model;
@end

@implementation AGTStarWarsUniverseViewController

-(id) initWithModel:(AGTStarWarsUniverse*) model
              style:(UITableViewStyle) style{
    
    if (self = [super initWithStyle:style]) {
        
        _model = model;
        self.title = @"StarWars Universe";
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    // Registro la celda personalizada
    [self registerNibs];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == IMPERIAL_SECTION) {
        return [self.model imperialCount];
    }else{
        return [self.model rebelCount];
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguar de qué personaje me habla y qué tipo es
    UIImage *side = nil;
    AGTStarWarsCharacter *character = nil;
    if (indexPath.section == IMPERIAL_SECTION) {
        character = [self.model imperialAtIndex:indexPath.row];
        side = [UIImage imageNamed:@"rebel.png"];
    }else{
        character = [self.model rebelAtIndex:indexPath.row];
        side = [UIImage imageNamed:@"empire.png"];
    }
    
    // Crear la celda
    AGTStarWarsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTStarWarsTableViewCell cellId] forIndexPath:indexPath];
    
    
    // Configurarla: sincronizar modelo (personaje) -> Vista (celda)
    cell.characterPhotoView.image = character.photo;
    cell.aliasView.text = character.alias;
    cell.nameView.text = character.name;
    cell.sideOfTheForceView.image = side;
    
    // Devolvemos la celda
    return cell;
    
}

-(CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [AGTStarWarsTableViewCell height];
}

-(NSString*) tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section{
    
    if (section == IMPERIAL_SECTION) {
        return @"Galactic Empire";
    }else{
        return @"Rebel Alliance";
    }
}

#pragma mark - TableView Delegate
-(void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguar de qué personaje me habla
    AGTStarWarsCharacter *character;
    if (indexPath.section == IMPERIAL_SECTION) {
        character = [self.model imperialAtIndex:indexPath.row];
    }else{
        character = [self.model rebelAtIndex:indexPath.row];
    }
    
    
    // Avisamos al delegado
    if ([self.delegate respondsToSelector:@selector(starWarsUniverseViewController:didSelectCharacter:)]) {
        
        [self.delegate starWarsUniverseViewController:self
                                   didSelectCharacter:character];
    }
    
    
    // Enviamos la notificación
    NSDictionary *dict = @{CHARACTER_KEY : character};
    NSNotification *n = [NSNotification notificationWithName:CHARACTER_DID_CHANGE_NOTIFICATION object:self userInfo:dict];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotification:n];
    
    
}


#pragma mark - AGTStarWarsUniverseViewControllerDelegate
-(void) starWarsUniverseViewController:(AGTStarWarsUniverseViewController *)vc
                    didSelectCharacter:(AGTStarWarsCharacter *)character{
    
    // Creo un characterVC
    AGTCharacterViewController *charVC = [[AGTCharacterViewController alloc] initWithModel:character];
    
    // Me hago un push
    [self.navigationController pushViewController:charVC
                                         animated:YES];
    
}

#pragma mark - Utils
-(void) registerNibs{
    
    UINib *nib = [UINib nibWithNibName:@"AGTStarWarsTableViewCell" bundle:nil];
    
    [self.tableView registerNib:nib
         forCellReuseIdentifier:[AGTStarWarsTableViewCell cellId]];
}








@end
