//
//  AGTStarWarsTableViewCell.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 28/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTStarWarsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *characterPhotoView;
@property (weak, nonatomic) IBOutlet UILabel *aliasView;
@property (weak, nonatomic) IBOutlet UILabel *nameView;
@property (weak, nonatomic) IBOutlet UIImageView *sideOfTheForceView;

+(NSString*) cellId;
+(CGFloat) height;

@end
